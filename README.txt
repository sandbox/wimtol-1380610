What is it?
-----------
IDFix is a configuration driven engine for building relational database
applications. The application is described in a php-array structure or a
configuration file. The IDFix engine than reads this configuration and
builds the lists and editforms for you.

Why not use the node system and views?
---------------------------------------
Looking at Drupal you can see roughly two layers. The real core
consisting of the bootstrap and the different API's, and on the
other hand the core-modules. The ones you see on the modules
page in the user-interface. These core modules (especcialy node
and comment) make up the CMS functionality of Drupal.
For building websites this is a fine solution. But what if you
want to build a more data-oriented site? Of course you can use
the node system, add the new FIELD API and Views in the mix and
you have a powerfull solution. But..... focussed on content!
The node system is sometimes just to bulky for lean and mean
data. It's just not build for it. Thats's the bottomline. The
node system is perfect for content. But not for thousands of
records of data.
And that's where IDFix comes in. It's basicly a replacement
layer for the node system. It uses the real Drupal core as a
PHP framework to build upon. Bypassing every  other add-on
core module. It adds to Drupal the possibility to build real
lean and mean business and data oriented backoffice applications.
On steroids!

Is that all?
------------
But that's not all. Agile is the magic word! We need to build
functional prototypes in hours these days. Well, at our company we do!
So IDFix not only gives you a new way to build your application, but
it also provides you with a new scripting language! Don't be afraid, it's
so easy te learn! And if you don't want it? Create a module and specify
the configuration as a PHP array. No problem.
And if you have your prototype it's easy to build upon that. IDFix uses
the familiar Drupal hook system to build a powerfull eventhandler system.

Example?
--------
Let's show you a configuration for a very simple application for storing
telephone numbers and names. Here we go:

-title=Telephone Numbers
-description=Sample application for storing telephone numbers
#tables
  #numbers
    -id=10
    -title=Numbers
    -description=Store your favourite names and numbers
    #search
      -Name
    #fields
      -Number
      -Name

That's it. Your first IDFix application. Two fields. The possibility
to search. A list view, and an editform. All you need. I typed it in about
two minutes.
And this application is fully functional!! Store 500.000 records in it.
No problem.
Try this with the node-system, CCK and Views. You can do it. No problem.
But can you do it in 2 minutes? And can it handle 500.000 records plus?
Mww.... maybe. But advisable?

Do I need it?
-------------
If you are building content websites, no you probably don't. But IDFix
makes it possible to use Drupal in corporate environments, builidng web based
internal applications that were earlier the domain of .Net, powerbuilder,
Visual Foxpro, Visual Basic, and all those others.
And maybe, maybe even some SAAS applications. Who knows?

What's in the pipeline?
-----------------------
Already there are some add-on modules for IDFix. Because an IDFix
application is basicly just a configuration file you should be able to push
it quite easily to an other server? And that's just what IDFix_deploy does.
Creating an API for IDFix and other modules to push data by way of the
Drapal XMLRPC interface to other servers. Ain't that agile?
But there is already a treeview add-on, an eventhandler scripting add-on
that hooks into IDFix_deploy and a couple of more .....

What�s new in 7?
----------------
This module was named Asterix in Drupal 6. IDFix has much of the same
functionality, but a completely new architecture. More flexibility, more
events, more granularity. It's also got less code! And it takes advantage
of the new Drupal 7 functionality where possible. Last but not least the
configuration file/array is fully compatible with Asterix!

Credits
-------
IDFix was partially made possible by Cendris, The Netherlands.

Wim Tol
December 2011
