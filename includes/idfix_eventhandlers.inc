<?php

/**
 * @file
 * Implemented eventhandlers
 *
 */

/**
 * Implements hook_idfix_event_get_config_cache().
 *
 * @param array &$config
 *   IDFix configuration
 * @param string $config_name
 *   Name of the configuration
 */
function idfix_idfix_event_check_config_cache(&$config, $config_name) {
  idfix_profiler(__FUNCTION__, 'start');
  // Create a default table-id
  static $id_counter = 500;

  // Defaults for title and description.
  _idfix_set_default_config_value($config, 'title', t('Untitled idfix Application'));
  _idfix_set_default_config_value($config, 'description', t('Description for this untitled idfix application'));

  // What table do we need to use?
  $tablespace = _idfix_api_valid_identifier($config_name);
  _idfix_set_default_config_value($config, 'tablespace', $tablespace);

  // Always prefix it with idfix_.
  if (substr($config['tablespace'], 0, 6) != 'idfix_') {
    $config['tablespace'] = 'idfix_' . $config['tablespace'];
  }

  // Create it if it doesn't exist already.
  _idfix_api_build_tablespace($config['tablespace']);
  _idfix_set_default_config_value($config, '_name', $config_name);


  if (isset($config['tables']) and is_array($config['tables'])) {
    foreach ($config['tables'] as $table_name => &$table_config) {

      // Process table properties.
      _idfix_set_default_config_value($table_config, '_name', $table_name);
      _idfix_set_default_config_value($table_config, 'title', $table_name);
      _idfix_set_default_config_value($table_config, 'description', t('Description for table !table', array('!table' => $table_config['title'])));

      // Default ID.
      if (!array_key_exists('id', $table_config)) {
        _idfix_set_default_config_value($table_config, 'id', $id_counter += 10);
        idfix_debug(t('No ID set for table %table. Default ID created #%id', array('%table' => $table_name, '%id' => $id_counter)), t('If you dont wanna loose any information already in the system, use this ID in your configuration file. If you change the ID al information will be lost but not deleted. Setting the ID to the original value will make sure all information is restored.'), IDFIX_TRAIL_ERROR);
      }

      if (isset($table_config['fields']) and is_array($table_config['fields'])) {
        foreach ($table_config['fields'] as $field_name => &$field_config) {
          // Process field properties.
          _idfix_set_default_config_value($field_config, 'title', $field_name);
          _idfix_set_default_config_value($field_config, '_name', $field_name);
          _idfix_set_default_config_value($field_config, '_tablename', $table_name);
        }
      }
    }
  }
  idfix_profiler(__FUNCTION__, 'stop');
}


/**
 *
 * S U P P O R T   F U N C T I O N S
 *
 */

/**
 * _idfix_set_default_config_value()
 *
 * Set an optional value in the configuration array.
 * If the value is an array, every element of that array is checked also
 * We could have used recursion here, but that's not so performance friendly
 * and we only need one level at this moment.
 *
 * @param array &$config
 *   Configuration array to set the default value
 * @param string $key
 *   Index in the configuration array
 * @param string|int|array $value
 *   Default value to set
 */
function _idfix_set_default_config_value(&$config, $key, $value) {
  idfix_profiler(__FUNCTION__, 'start');

  // Multiple config values to check.
  if (is_array($value)) {
    foreach ($value as $check_key => $set_value) {
      if (!isset($config[$key][$check_key])) {
        $config[$key][$check_key] = $set_value;
        idfix_debug('Default config-value set', "'$key'=>'$check_key'=>'$set_value'", IDFIX_TRAIL_VERBOSE);
      }
    }
  }
  elseif (!isset($config[$key])) {
    $config[$key] = $value;
    idfix_debug('Default config-value set', "'$key'=>'$value'", IDFIX_TRAIL_VERBOSE);
  }

  idfix_profiler(__FUNCTION__, 'stop');
}
