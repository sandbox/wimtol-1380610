<?php

/**
 * @file
 *
 * Eventhandling system
 *
 */

/**
 * Get a full list of supported events
 *
 * @todo
 *   Implement persistent caching
 *
 * @return array
 *   List of events and descriptions
 */
function idfix_api_get_events() {
  $events = array();
  idfix_api_eventhandler($events, 'get_events');
  return $events;
}

/**
 * Implements hook_idfix_event_get_events().
 *
 * @param array &$events
 *   The list of events to change
 */
function idfix_idfix_event_get_events(&$events) {
  $events['get_events'] = t('Get a list of implemented events and their descriptions');
  $events['get_config_cache'] = t('Deliver or change a configuration. The result is cached.');
  $events['get_config_nocache'] = t('Change/postprocess a configuration. The result is not cached.');
  $events['idfix_eventhandler'] = t('Global eventhandler.');
}

/**
 * idfix_api_eventhandler()
 *
 * IDF has it's own eventhandling system that automates the task of
 * calling events on field, table and idfix level.
 * Each event for a specific operator table and fieldname is called
 * only once every pageview
 *
 * Use this system instead of their drupal counterparts like:
 * module_invoke
 * module_invoke_all
 * drupal_alter
 *
 * @param array &$data
 *   Data structure to change
 * @param string $op
 *   name of the event
 * @param string $config_name
 *   name of the config
 * @param string $tablename
 *   Name of the table
 * @param string $fieldname
 *   Name of the field
 * @param array $record
 *   Optinal data record
 */
function idfix_api_eventhandler(&$data, $op, $config_name = '', $tablename = '', $fieldname = '', $record = array()) {
  idfix_profiler(__FUNCTION__, 'start');

  // First stage, call the fieldlevel event.
  if ($fieldname) {
    $hook = 'idfix_event_' . $op . '_' . $tablename . '_' . $fieldname;
    _idfix_api_eventhandler($data, $hook, $config_name, $tablename, $fieldname, $record);
  }

  // Second stage, call the tablelevel event.
  if ($tablename) {
    $hook = 'idfix_event_' . $op . '_' . $tablename;
    _idfix_api_eventhandler($data, $hook, $config_name, $tablename, $fieldname, $record);
  }

  // Third stage, call the asterix level event.
  $hook = 'idfix_event_' . $op;
  _idfix_api_eventhandler($data, $hook, $config_name, $tablename, $fieldname, $record);

  // Last stage, custom eventhandler implementation.
  $hook = 'idfix_eventhandler';
  foreach (module_implements($hook) as $module_name) {
    // Second run the function.
    $function = $module_name . '_' . $hook;
    $arguments = array(&$data, $op, $config_name, $tablename, $fieldname, $record);
    call_user_func_array($function, $arguments);
  }

  idfix_profiler(__FUNCTION__, 'stop');
}

/**
 * _idfix_api_eventhandler()
 *
 * Low level implementation of an eventahandler system call
 * Code is similar to drupal_alter
 *
 * @param array &$data
 *   Data structure to change
 * @param string $op
 *   name of the event
 * @param string $config_name
 *   name of the config
 * @param string $tablename
 *   Name of the table
 * @param string $fieldname
 *   Name of the field
 * @param array $record
 *   Idfix data record
 */
function _idfix_api_eventhandler(&$data, $op, $config_name, $tablename, $fieldname, $record) {
  // Run the events.
  foreach (module_implements($op) as $module_name) {
    // Second run the function.
    $function = $module_name . '_' . $op;
    $arguments = array(&$data, $config_name, $tablename, $fieldname, $record);
    call_user_func_array($function, $arguments);
  }
}
