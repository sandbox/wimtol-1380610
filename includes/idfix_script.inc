<?php

/**
 * @file
 * @author wtol
 * @copyright 2011
 *
 * Simple structure parser but foolproof for easy configuration editing
 *
 * Tip: Use javascript syntax highlighting
 */


/**
 * idfix_scriptidfix_script_parse()
 *
 * Parse an idfix configuration file
 * Keyproint for the configuration is flexibility,
 * ease of use and forgiving syntax
 *
 *
 * @param string $filename
 *   Full path to the configuration file
 * @param int $level
 *   Only used with recursion
 *
 * @return array
 *   Parsed config
 */
function idfix_script_parse($filename, $level = -1) {
  $retval = array();

  while (TRUE) {
    $row = _idfix_script_rowreader($filename);
    // Exit if no row found.
    if (!$row) {
      break;
    }

    // Check if it is an array.
    if ($row['token'] == '#' and $row['key']) {
      // If it is indented, we have a new structure.
      if ($row['level'] > $level) {
        // Create new array.
        $retval[$row['key']] = idfix_script_parse($filename, $row['level']);
      }
      // Otherwise we need to go back a level.
      else {
        /* Signal that the row is not yet parsed
           and needs to be analyzed again */
        _idfix_script_rowreader($filename, TRUE);
        break;
      }

    }
    // Check if it is an item.
    elseif ($row['token'] == '-') {
      /* If key and value are the same, the rowreader has detected
         just a single value after the token. In that case we rather
         have a default numeric indexed arraykey. */
      if ($row['key'] == $row['value']) {
        $retval[] = $row['value'];
      }
      // If there's no key, noo need to store this value.
      elseif ($row['key']) {
        $retval[$row['key']] = $row['value'];
      }
    }
    /* None of these conditions?
       Than it's whitespace or comment! */
  }

  // Close the file if we are on the main entry level.
  if ($level === -1) {
    _idfix_script_rowreader('RESET');
  }

  return $retval;
}

/**
 * _idfix_script_rowreader()
 *
 * Read one row from an idfix configutration file
 * and return the analysed row as a structured array
 *
 * @param string $filename
 *   Full path to the config file
 * @param bool $setflag
 *   If true read this line again
 *
 * @return array|null
 *   Tokenized config line
 */
function _idfix_script_rowreader($filename, $setflag = FALSE) {
  static $filehandle = NULL;
  static $flag_readline_again = FALSE;
  static $prev_row = array();

  /* If filename is NULL we need to close the file and
     reset the filhandle for the next file. */
  if ($filename === 'RESET' and $filehandle) {
    fclose($filehandle);
    $filehandle = NULL;
    return;
  }

  // We only set the flag if asked.
  if ($setflag) {
    $flag_readline_again = TRUE;
    return;
  }

  // Need the line again?
  if ($flag_readline_again) {
    $flag_readline_again = FALSE;
    return $prev_row;
  }

  $retval = NULL;
  // Open file.
  if (is_NULL($filehandle)) {
    $filehandle = fopen($filename, 'r');
  }

  // Read next row.
  if ($filehandle) {
    if ($line = fgets($filehandle, 4096)) {
      $retval = _idfix_script_lineparser($line);
      // Store it in case we need it again.
      $prev_row = $retval;
    }
  }

  return $retval;
}

/**
 * _idfix_script_lineparser()
 *
 * Parse a line from an idfix configuration
 *
 * Line has the following structure:
 * [optional whitespace][token # or -][optional identifier][optional =][value]
 *
 * If there is no token the line is handled as comment
 * Inline comments are NOT allowed at this moment
 *
 * @param string $line
 *   Line from the file
 *
 * @return array
 *   Tokenized line
 */
function _idfix_script_lineparser($line) {
  $token = (string) drupal_substr(trim($line), 0, 1);
  if (!$token) {
    $token = ' ';
  }
  $level = (integer) strpos($line, $token);
  $data = trim(drupal_substr($line, $level + 1));

  // Check separator for $key/value pair.
  if ($separator = strpos($data, '=')) {
    $key = drupal_substr($data, 0, $separator);
    $value = drupal_substr($data, $separator + 1);
  }
  else {
    $key = $data;
    $value = $data;
  }

  // Make the key an integer if needed.
  if (is_numeric($key)) {
    $key = (integer) $key;
  }
  // ... otherwise turn the key into a valid identifier.
  else {
    $key = _idfix_api_valid_identifier($key);
  }


  return array('token' => $token, 'level' => $level, 'key' => $key, 'value' => $value);
}
