<?php

/**
 * @file
 * Profiling and debugging
 *
 */

/**
 * Messages to follow the flow of IDFix processing
 */
define('IDFIX_TRAIL_VERBOSE', 1);

/**
 * Messages for debugging IFDIx processing
 */
define('IDFIX_TRAIL_DEBUG', 2);

/**
 * Messages to track errors
 */
define('IDFIX_TRAIL_ERROR', 3);

/**
 * Turn off profiling and debugging.
 * Use this setting for production sites.
 */
define('IDFIX_TRAIL_PRODUCTION', 9);

/**
 * idfix_profiler()
 *
 * @param string $name
 *   Name of the function we need to profile
 * @param string $op
 *   Start or stop or render
 *
 * @return null|string
 *   If op=render a rendered HTML list is returned
 */
function idfix_profiler($name, $op = 'start') {
  // Don't spend any time here if not strictly nescesary.
  if (!_idfix_profiler()) {
    return;
  }
  // No cache API used. Recursion!
  static $timers = array();
  $return = NULL;

  if ($op == 'start') {
    $timers[$name]['start'] = (float) microtime(TRUE);
    // Initialize the two other elements.
    if (!isset($timers[$name]['count'])) {
      $timers[$name]['count'] = 0;
      $timers[$name]['total'] = 0;
    }
  }
  elseif ($op == 'stop') {
    $timers[$name]['count']++;
    $diff = (float) microtime(TRUE) - $timers[$name]['start'];
    $timers[$name]['total'] += (float) $diff;
  }
  elseif ($op == 'render') {
    $header = array(t('name'), t('count'), t('time'));
    $data = array();
    foreach ($timers as $timer_name => $timer_data) {
      $row = array($timer_name, $timer_data['count'], round($timer_data['total'] * 1000, 1));
      $data[] = $row;
    }
    $variables = array('header' => $header, 'rows' => $data);
    $return = theme('table', $variables);
  }


  return $return;
}

/**
 * idfix_debug()
 *
 * @param string $message
 *   The text to show in the display
 * @param array $vars
 *   The parameters to show in the second column
 * @param int $type
 *   Type of message as defined above this function
 *
 * @return string|array|null
 *   Full HTML or array with the complete table of messages
 *   Return null in case we don't have access.
 */
function idfix_debug($message = NULL, $vars = NULL, $type = IDFIX_TRAIL_VERBOSE) {
  idfix_profiler(__FUNCTION__, 'start');

  // No User access!
  if (!user_access('administer idfix')) {
    idfix_profiler(__FUNCTION__, 'stop');
    return NULL;
  }

  if (is_null($message)) {
    $list = array();
    if (isset($_SESSION['idfix']['debugtrail'])) {
      $list = $_SESSION['idfix']['debugtrail'];
      unset($_SESSION['idfix']['debugtrail']);
    }
    idfix_profiler(__FUNCTION__, 'stop');
    return $list;
  }

  // Only display relevant debug information.
  if ($type < _idfix_debug_level()) {
    idfix_profiler(__FUNCTION__, 'stop');
    return NULL;
  }

  // Initialize the list.
  if (!isset($_SESSION['idfix']['debugtrail'])) {
    $_SESSION['idfix']['debugtrail'] = array();
  }

  if (!is_NULL($message)) {
    $messages = _idfix_get_debugtrail_list();
    $code = array(
      'data' => $messages[$type],
      'class' => 'debug-trail debug-trail-' . $type,
      );
    $_SESSION['idfix']['debugtrail'][] = array(timer_read('page'), $code, $message, print_r($vars, TRUE));
  }

  idfix_profiler(__FUNCTION__, 'stop');
  return $_SESSION['idfix']['debugtrail'];
}
/**
 * Supporting functions.
 *
 */

/**
 * Get the current debug level.
 *
 * If there is no proper permission the default return value
 * will be set to production mode and no messages will be shown.
 *
 * @return integer
 *   Current debug level
 */
function _idfix_debug_level() {
  if (user_access('administer idfix')) {
    $return = (integer) variable_get('idfix_debug_level', IDFIX_TRAIL_VERBOSE);
  }
  else {
    $return = (integer) IDFIX_TRAIL_PRODUCTION;
  }
  return $return;
}

/**
 * _idfix_profiler()
 *
 * @return integer
 *   Turn profiling on or off
 */
function _idfix_profiler() {
  return (boolean) (variable_get('idfix_profiler', 0) and user_access('administer idfix'));
}

/**
 * _idfix_get_debugtrail_list()
 *
 * @return array
 *   List of options for the debugtrail
 */
function _idfix_get_debugtrail_list() {
  $debug_options = idfix_api_cache_get(__FUNCTION__);
  if (is_NULL($debug_options)) {
    $debug_options = array(
      IDFIX_TRAIL_VERBOSE => t('Verbose'),
      IDFIX_TRAIL_DEBUG => t('Debug'),
      IDFIX_TRAIL_ERROR => t('Error'),
      IDFIX_TRAIL_PRODUCTION => t('No Info, production mode'),
      );
    idfix_api_cache_set(__FUNCTION__, $debug_options);
  }

  return $debug_options;
}
