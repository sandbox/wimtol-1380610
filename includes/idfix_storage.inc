<?php

/**
 * @file
 * Include file for all storage functions
 *
 */

/**
 * Load a single record from the table based on the Unique ID
 *
 * @param string $config_name
 *   Name of the configuration
 * @param integer $main_id
 *   Unique identifier to the record
 *
 * @return array
 *   Record from the datastore or empty array
 */
function idfix_api_load_one($config_name, $main_id) {
  $set = _idfix_api_load($config_name, array('mainid' => $main_id));
  return array_shift($set);
}

/**
 * This is the workhorse function to get to a set of records
 * from the IDFix table.
 *
 * Most of the time you will be using the simplified wrapper functions.
 *
 * @param string $config_name
 *   Name of the configuration
 * @param array $filter
 *   Key/value pairs to create conditions
 * @param array $sort
 *   Fieldname(s) to sort on
 * @param integer $limit
 *   Maximum number of rows to return. Defaults to zero (=ALL).
 * @param bool $cache
 *   Should we cache the resultset?
 * @param bool $tablesort
 *   Should we include the tablesort extender?
 * @param bool $pager
 *   Should we include the pager extender?
 *
 * @return array
 *   List of records from the datastore
 */
function _idfix_api_load($config_name, $filter = array(), $sort = array('Weight'), $limit = 0, $cache = TRUE, $tablesort = FALSE, $pager = FALSE) {
  idfix_profiler(__FUNCTION__, 'start');
  // First things first, check the cache!
  if ($cache) {
    $cache_key = md5(serialize(array($config_name, $filter, $sort, $limit)));
    $return = idfix_api_cache_get($cache_key);
    if (!is_null($return)) {
      return $return;
    }
  }
  // Set default return value.
  $return = array();

  // Create the query object.
  $config = idfix_api_get_config($config_name);
  $tablespace = $config['tablespace'];
  $query = db_select($tablespace);
  idfix_api_eventhandler($query, 'query_pre', $config_name);


  // Tablesort extender.
  if ($tablesort) {
    $query = $query->extend('TableSort');
  }

  // Pager extender.
  if ($pager) {
    $query = $query->extend('PagerDefault');
  }

  // Add all fields.
  $query = $query->fields($tablespace);

  // Add the filters.
  if (is_array($filter)) {
    foreach ($filter as $field_name => $field_value) {
      $query = $query->condition($field_name, $field_value);
    }
  }

  // Do we need sorting?
  if (is_array($sort)) {
    foreach ($sort as $field_name) {
      $query = $query->orderBy($field_name);
    }
  }

  // Limitting?
  if ($limit) {
    $query = $query->limit($limit);
  }

  // Last chance to modify the query object.
  idfix_api_eventhandler($query, 'query_post', $config_name);

  // Process the query and it's results.
  $result = $query->execute();
  foreach ($result as $row) {
    // Postprocess the data blob.
    $data = (array) $row;
    $properties = (array) unserialize($data['data']);
    $data += $properties;
    unset($data['data']);
    // Store the configuration name in the record.
    $data['_config'] = $config_name;
    // Postprocess the full row.
    idfix_api_eventhandler($data, 'read_row', $config_name);
    // And store it in the return array.
    $return[] = $data;
  }

  // Modify the full dataset.
  idfix_api_eventhandler($return, 'read_rowset', $config_name);

  // Do we need to cache this data?
  if ($cache) {
    idfix_api_cache_set($cache_key, $return);
  }

  idfix_profiler(__FUNCTION__, 'stop');
  return $return;
}

/**
 * idfix_api_save()
 *
 * Save a complete record in the idfix table
 *
 * @param array $data
 *   Full data record to save
 * @param string $config_name
 *   Configuration name
 *
 * @return integer
 *   mainid of the newly saved or updated record
 */
function idfix_api_save($data, $config_name) {
  idfix_profiler(__FUNCTION__, 'start');

  global $user;

  // Set some default values.
  $data['uidchange'] = $user->uid;
  $data['tschange'] = time();

  idfix_api_eventhandler($data, 'save', $config_name);

  if (isset($data['mainid']) AND $data['mainid']) {
    $retval = _idfix_api_save_update($data, $config_name);
  }
  else {
    $retval = _idfix_api_save_new($data, $config_name);
  }


  idfix_profiler(__FUNCTION__, 'stop');
  return $retval;
}

/**
 * _idfix_api_save_update()
 *
 * Do a record update on the idfix table
 *
 * @param array $fields
 *   Values to save
 * @param string $config_name
 *   Name of the config
 *
 * @return int
 *   UID of the saved record
 */
function _idfix_api_save_update($fields, $config_name) {
  idfix_profiler(__FUNCTION__, 'start');

  idfix_api_eventhandler($fields, 'save_update', $config_name);

  // Save the return value and unset.
  $retval = (integer) $fields['mainid'];
  unset($fields['mainid']);

  // Fetch the unique tablename for this config.
  $tablespace = idfix_api_get_tablespace($config_name);

  // Now separate the properties and fields.
  _idfix_api_separate_fieldlist($fields, $tablespace);

  $query = db_update($tablespace)
    ->condition('mainid', $retval)
    ->fields($fields);
  idfix_api_eventhandler($query, 'query_save_update', $config_name);
  $query->execute();

  idfix_profiler(__FUNCTION__, 'stop');
  return $retval;
}

/**
 * _idfix_api_save_new()
 *
 * Add a new record to the table
 *
 * @param array $fields
 *   Values to save
 * @param string $config_name
 *   name of the config
 *
 * @return int
 *   UID of the new idfix record
 */
function _idfix_api_save_new($fields, $config_name) {
  idfix_profiler(__FUNCTION__, 'start');

  global $user;

  $tablespace = idfix_api_get_tablespace($config_name);

  // It is a new record! Set some defaults.
  $fields['tscreate'] = time();
  $fields['uidcreate'] = $user->uid;

  // Trigger the events.
  idfix_api_eventhandler($fields, 'save_new', $config_name);

  // Now separate the properties and fields.
  _idfix_api_separate_fieldlist($fields, $tablespace);

  $query = db_insert($tablespace)->fields($fields);
  idfix_api_eventhandler($query, 'query_save_new', $config_name);
  $retval = (integer) $query->execute();

  idfix_profiler(__FUNCTION__, 'stop');
  return $retval;
}

/**
 * _idfix_get_fieldlist()
 *
 * @param string $tablespace
 *   Name of the data table
 *
 * @return array
 *   Fieldlist
 */
function _idfix_api_get_fieldlist($tablespace) {
  idfix_profiler(__FUNCTION__, 'start');

  // Retrieve the cached list.
  $cache = idfix_api_cache_get(__FUNCTION__);

  if (!$cache) {
    // Using the tablespace directly in the query is save.
    // It is fully under control by idfix.
    $result = db_query("SHOW fields FROM {{$tablespace}}");

    $cache = array();
    foreach ($result as $row) {
      $cache[$row->field] = $row->field;
    }

    idfix_api_cache_set(__FUNCTION__, $cache);
  }

  idfix_profiler(__FUNCTION__, 'stop');
  return $cache;
}

/**
 * _idfix_api_separate_fieldlist()
 *
 * Separate the properties from the real fields
 *
 * @param array &$fields
 *   idfix data record to process
 * @param string $tablespace
 *   Name of the data table
 */
function _idfix_api_separate_fieldlist(&$fields, $tablespace) {
  idfix_profiler(__FUNCTION__, 'start');
  $properties = array();
  $fieldlist = _idfix_api_get_fieldlist($tablespace);
  /* Check all fields. If it's a real field, save it.
     If it's not a real field add it to the property list
     and remove it from the fieldlist. */
  foreach ($fields as $fieldname => $fieldvalue) {
    if (!isset($fieldlist[$fieldname])) {
      // It's a property!!!!
      $properties[$fieldname] = $fieldvalue;
      unset($fields[$fieldname]);
    }
  }
  // Now store the serialized version in the data element.
  $fields['data'] = serialize($properties);
  idfix_profiler(__FUNCTION__, 'stop');
}
/**
 * Delete the full trail of IDFix objects from the datastore
 *
 * @param string $config_name
 *   name of the configuration
 * @param integer $main_id
 *   Unique IDFIx object-id
 */
function idfix_api_delete($config_name, $main_id) {
  idfix_profiler(__FUNCTION__, 'start');
  $config = idfix_api_get_config($config_name);
  _idfix_api_delete_recurse($config, $main_id);
  idfix_profiler(__FUNCTION__, 'stop');
}

/**
 * Recursive internal function for deleting IDFix objects
 *
 * @param array $config
 *   Configuration
 * @param integer $main_id
 *   Unique IDFIx object-id
 */
function _idfix_api_delete_recurse($config, $main_id) {
  $tablespace = $config['tablespace'];

  // First delete the parents.
  $result = db_select($tablespace, 'i')
    ->fields('i', array('mainid'))
    ->condition('parentid', $main_id)
    ->execute();

  foreach ($result as $record) {
    _idfix_api_delete_recurse($config, $record->mainid);
  }

  // Trigger an event just before the delete.
  idfix_api_eventhandler($main_id, 'delete', $config['_name']);

  /* Note: It is possible to prevent the deletion by setting
     $main_id to zero or another non existing ID */

  // Second delete the item itself.
  db_delete($tablespace)
    ->condition('mainid', $main_id)
    ->execute();

  idfix_debug(__FUNCTION__, array($tablespace, $main_id));
}

/**
 * Copy objects from the IDFix datastore
 *
 * @param string $config_name
 *   name of the configuration
 * @param integer $main_id
 *   Unique IDFIx object-id
 *
 * @return integer
 *   New ID of the copied parent record
 */
function idfix_api_copy($config_name, $main_id) {
  idfix_profiler(__FUNCTION__, 'start');
  $record = idfix_api_load_one($config_name, $main_id);

  /* If the default "Name" field is used, indicate that this is in
     fact a copy. */
  if (isset($record['name']) and $record['name']) {
    $record['name'] .= ' ' . t('(copy)');
  }

  $return = _idfix_api_copy_recurse($record, $record['parentid'], $config_name);
  idfix_profiler(__FUNCTION__, 'stop');
  return $return;
}

/**
 * Recursive function for copying batches of idfix objects
 *
 * @param array $record
 *   Data to copy
 * @param integer $parent_id
 *   Unique identifier from the parent
 * @param string $config_name
 *   Name of the configuration
 *
 * @return integer
 *   New ID of the copied parent record
 */
function _idfix_api_copy_recurse($record, $parent_id, $config_name) {
  $main_id = (integer) $record['mainid'];
  unset($record['mainid']);
  $record['parentid'] = $parent_id;
  $new_main_id = idfix_api_save($record, $config_name);

  // Now get all child records.
  $childs = _idfix_api_load($config_name, array('parentid' => $main_id));
  foreach ($childs as $child_id => $child_record) {
    _idfix_api_copy_recurse($child_record, $new_main_id, $config_name);
  }
  return $new_main_id;
}
/**
 * SUPPORT FUNCTIONS
 *
 */

/**
 * idfix_api_build_tablespace()
 *
 * Make a copy of the idfix table if we need a new tablespace
 *
 * @param string $tablespace
 *   Name of the idfix data table
 */
function _idfix_api_build_tablespace($tablespace) {
  db_query("CREATE TABLE IF NOT EXISTS {{$tablespace}} LIKE {idfix}")->execute();
}

/**
 * _idfix_api_valid_identifier()
 *
 * Build a valid identifier from any given string
 * - Lowercase
 * - Alfabetic values
 * - Numeric values
 * - Underscores
 * - Not starting with a number
 *
 * @param string $key
 *   Value to clean
 *
 * @return string
 *   Processed identifier
 */
function _idfix_api_valid_identifier($key) {
  idfix_profiler(__FUNCTION__, 'start');

  $key = strtolower($key);
  $blacklist = str_replace(str_split('abcdefghijklmnopqrstuvwxyz_1234567890'), '', $key);
  if ($blacklist) {
    $key = str_replace(str_split($blacklist), '_', $key);
  }
  if (is_numeric(drupal_substr($key, 0, 1))) {
    $key = '_' . $key;
  }
  idfix_profiler(__FUNCTION__, 'stop');
  return $key;
}

/**
 * Just return the tablespace name
 *
 * @param string $config_name
 *   Name of the configuration
 *
 * @return string
 *   Tablename where the data is stored
 */
function idfix_api_get_tablespace($config_name) {
  $cache_key = __FUNCTION__ . $config_name;
  if (!($return = idfix_api_cache_get($cache_key))) {
     $config = idfix_api_get_config($config_name);
     $return = $config['tablespace'];
     idfix_api_cache_set($cache_key, $return);
  }
  return $return;
}
