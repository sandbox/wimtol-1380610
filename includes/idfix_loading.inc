<?php

/**
 * @file
 * Load idfix configurations
 *
 */

/**
 * Load a configuration array.
 *
 * This function is the workhorse from idfix. It uses a two level
 * caching stategy and fires events for getting the configuration.
 * Main difference with idfix is the postprocessing of the configuration.
 *
 * Note: Use the get_config_nocache event with care.
 *
 * @param string $config_name
 *   Name of the configuration
 * @param array $record
 *   Optional record from the datastore
 *
 * @return array
 *   Fully processed and cached configuration array
 */
function idfix_api_get_config($config_name, $record = array()) {
  idfix_profiler(__FUNCTION__, 'start');

  // Unique cache key.
  $cache_key = __FUNCTION__ . '_' . $config_name;

  // First check the cache.
  $config = idfix_api_cache_get($cache_key);

  if (is_array($config)) {
    idfix_debug('Config read from DRUPAL cache', $cache_key, IDFIX_TRAIL_VERBOSE);
  }
  else {
    // Always add the name of the config.
    $config = array();
    idfix_api_eventhandler($config, 'get_config_cache', $config_name);
    idfix_api_eventhandler($config, 'check_config_cache', $config_name);
    idfix_api_cache_set_persistent($cache_key, $config);
    idfix_debug('Config build new', $cache_key, IDFIX_TRAIL_VERBOSE);
  }

  // Parse dynamic values and functions.
  if (count($record) > 0) {
    $config = idfix_api_postprocess_config($config, $record, $config_name);
  }
  // Last fase, postprocess the configuration.
  idfix_api_eventhandler($config, 'get_config_nocache', $config_name, '', '', $record);

  idfix_profiler(__FUNCTION__, 'stop');
  return $config;
}

/**
 * idfix_api_get_table_config()
 *
 * Get a cached copy of a little part of the full configuration.
 * It is more efficient to call this function than to get the
 * full configuration en extract your part. This is because less
 * postprocessing is needed by using this function, and the caching
 * can be done more efficiently.
 *
 * @param string $config_name
 *   Name of the configuration
 * @param string $table_name
 *   Name of the table
 * @param array $record
 *   Record from the data store for postprocessing
 *
 * @return array
 *   Table configuration
 */
function idfix_api_get_table_config($config_name, $table_name, $record = array()) {
  idfix_profiler(__FUNCTION__, 'start');
  // Build a key to the caching system.
  $cache_key = __FUNCTION__ . $config_name . $table_name . (isset($record['mainid']) ? $record['mainid'] : '');
  $table_config = idfix_api_cache_get($cache_key);

  // Let's build the config, but just once.
  if (!$table_config) {
    // Set a default config.
    $table_config = array();
    // Get a full config WITHOUT postprocessing!
    $config = idfix_api_get_config($config_name);
    if (isset($config['tables'][$table_name])) {
      // This is the one we need.
      $table_config = $config['tables'][$table_name];
      // Now do our own postprocessing if needed.
      if (count($record) > 0) {
        $table_config = idfix_api_postprocess_config($table_config, $record, $config_name);
      }
      // And store it for later use.
      idfix_api_cache_set($cache_key, $table_config);
    }
  }
  idfix_profiler(__FUNCTION__, 'stop');
  return $table_config;
}

/**
 * idfix_api_get_field_config()
 *
 * Get a fully cached copy of a field configuration.
 *
 * @param string $config_name
 *   Name of the configuration
 * @param string $table_name
 *   Name of the table
 * @param string $field_name
 *   Name of the field
 * @param array $record
 *   Record from the data store for postprocessing
 *
 * @return array
 *   Table configuration
 */
function idfix_api_get_field_config($config_name, $table_name, $field_name, $record = array()) {
  idfix_profiler(__FUNCTION__, 'start');
  // Get table config, it is already fully cached.
  $table_config = idfix_api_get_table_config($config_name, $table_name, $record);
  // Default field config.
  $field_config = array();
  // Make sure this is a valid field.
  if (isset($table_config['fields'][$field_name])) {
    $field_config = $table_config['fields'][$field_name];
  }
  idfix_profiler(__FUNCTION__, 'stop');
  return $field_config;
}

/**
 * idfix_api_read_config_file()
 *
 * This function is typically used from an event handler
 * in your own module: yourmodule_idfix_event_get_config_cache
 *
 * Note that although it may seem that you have got a valid idfix
 * config by calling this function, no additional processing is done
 * on the configuration.
 *
 * So, always use the api function idfix_api_get_config fetch a config,
 * than implement the eventhandler to modify or fill the configuration
 * structure.
 *
 * @param string $filename
 *   Valid linux based filename and path
 *
 * @return array
 *   Configuration read from the file
 *
 */
function idfix_api_read_config_file($filename) {
  idfix_profiler(__FUNCTION__, 'start');
  require_once 'idfix_script.inc';
  $return = idfix_script_parse($filename);
  idfix_profiler(__FUNCTION__, 'stop');
  return $return;
}

/**
 * idfix_api_postprocess_config()
 *
 * Postprocess a (part of a) configuration array for dynamic values
 *
 * Check for:
 * 1. Callbacks (with optional parameters)
 * 2. variables
 *
 * Variables are names surrounded by an % like '%ParenID%'
 * These are substituted with the values from the second
 * parameter to this function: $record
 *
 * @param array $config
 *   idfix configuration array
 * @param array $record
 *   idfix data record
 * @param string $config_name
 *   Name of the configuration
 *
 * @return array
 *   Processed configuration
 */
function idfix_api_postprocess_config($config, $record = array(), $config_name = '') {
  idfix_profiler(__FUNCTION__, 'start');

  if (is_array($config)) {
    foreach ($config as & $config_element) {
      // 1. Callback without parameters.
      if (is_string($config_element) AND (drupal_substr($config_element, 0, 1) == '@') AND function_exists(drupal_substr($config_element, 1))) {
        $config_element = call_user_func(drupal_substr($config_element, 1));

      }
      // 2. Callback with parameters.
      elseif (is_array($config_element) AND isset($config_element[0]) AND (drupal_substr($config_element[0], 0, 1) == '@') AND function_exists(drupal_substr($config_element[0], 1))) {
        // Get the function.
        $function = drupal_substr(array_shift($config_element), 1);
        // Now postprocess the parameters for dynamic values.
        $parameters = idfix_api_postprocess_config($config_element, $record);
        $config_element = call_user_func_array($function, $parameters);

      }
      // 3. Dynamic values to parse?
      elseif (is_string($config_element) and (stripos($config_element, '%') !== FALSE)) {
        $config_element = _idfix_parse_dynamic_values($config_element, $record);
      }
      // 4. Plain array? Recursive action.
      elseif (is_array($config_element)) {
        $config_element = idfix_api_postprocess_config($config_element, $record);
      }
    }
  }
  idfix_profiler(__FUNCTION__, 'stop');
  return $config;
}

/**
 * idfix_api_get_config_names()
 *
 * Ask different moduels for idfix configuration names they implement
 *
 * Calls upon idfix_event_idfix()
 *
 * @return array
 *   List with the supported configuration names
 */
function idfix_api_get_config_names() {
  // Let's build a default array.
  $config = array();
  idfix_api_eventhandler($config, 'idfix');
  return $config;
}

/**
 * Get a complete list of parent objects and their typeid/tablename
 *
 * @param int $main_id
 *   UID of an idfix record
 * @param string $config_name
 *   name of the configuration
 *
 * @return array
 *   Current trail
 */
function idfix_api_get_trail($main_id, $config_name) {
  idfix_profiler(__FUNCTION__, 'start');

  // Return cached trail if set.
  $cache_key = __FUNCTION__ . $config_name . $main_id;
  $cache = idfix_api_cache_get($cache_key);
  if ($cache) {
    return $cache;
  }

  $trail = array();

  $next_id = $main_id;
  while ($next_id) {
    $object = idfix_api_load_one($config_name, $next_id);
    $trail[$object['typeid']] = $object['mainid'];
    $next_id = $object['parentid'];
  }

  // Store the trail for later use.
  idfix_api_cache_set($cache_key, $trail);

  idfix_profiler(__FUNCTION__, 'stop');
  return $trail;

}

/**
 * Return the tablename by it's ID
 *
 * @param int $type_id
 *   Numeric type ID
 * @param string $config_name
 *   Name of the config
 *
 * @return array
 *   idfix table configuration array
 */
function idfix_api_get_tablename_by_id($type_id, $config_name) {
  idfix_profiler(__FUNCTION__, 'start');

  $cache_key = __FUNCTION__ . $config_name;
  $cache = idfix_api_cache_get($cache_key);

  if (!isset($cache[$type_id])) {
    $cache = array();
    $config = idfix_api_get_config($config_name);
    foreach ($config['tables'] as $table_name => $table_config) {
      $cache[ $table_config['id'] ] = $table_name;
    }
    idfix_api_cache_set_persistent($cache_key, $cache);
  }

  idfix_profiler(__FUNCTION__, 'stop');
  return $cache[$type_id];
}


/**
 * _idfix_parse_dynamic_values()
 *
 * @param string $haystack
 *   Value to process
 * @param array $values
 *   Search and replace values
 *
 * @return array
 *   Processed data structure
 */
function _idfix_parse_dynamic_values($haystack, $values) {
  idfix_profiler(__FUNCTION__, 'start');
  if (is_array($values)) {
    foreach ($values as $key => $value) {
      $search = '%' . $key . '%';
      if (strpos($haystack, $search) !== FALSE) {
        $haystack = str_replace($search, $value, $haystack);
      }
    }
  }
  idfix_profiler(__FUNCTION__, 'stop');
  return $haystack;
}
