<?php

/**
 * @file
 *
 * Drupal Settings
 */

/**
 * Return the system settings form
 */
function idfix_settings_form($form_state) {

  $debug_options = _idfix_get_debugtrail_list();

  $var_name = 'idfix_debug_level';
  $form[$var_name] = array(
    '#type' => 'radios',
    '#title' => t('How much debug information do you want?'),
    '#default_value' => variable_get($var_name, IDFIX_TRAIL_ERROR),
    '#description' => t('Print debug- and timer information at the bottom of the page. Configuration caching is disabled for profiler-, verbose- and debugmode'),
    '#options' => $debug_options,
    );

  $var_name = 'idfix_profiler';
  $form[$var_name] = array(
    '#type' => 'checkbox',
    '#title' => t('Show profiler information'),
    '#default_value' => variable_get($var_name, FALSE),
    '#description' => t('Show a table containing functions called and the time it took to process them.'),
    );

  return system_settings_form($form);
}
