<?php

/**
 * @file
 * Generic cache interface
 *
 * IDFix needs a lot of caching to perform speedy.
 * Mostly we need the two level caching of using static caching
 * as wel assql caching. The cache functions provide a stable
 * interface to the drupal caching API.
 *
 */

/**
 * Set a value in the static cache
 *
 * @param string $cid
 *   Unique cache identifier
 * @param string|array|object|integer|null $data
 *   Data to be cached.
 *
 * @return null
 *   Always returns null
 */
function idfix_api_cache_set($cid, $data) {
  return _idfix_cache_worker($cid, $data);
}

/**
 * idfix_api_cache_set_persistent()
 *
 * @param string $cid
 *   Unique cache identifier
 * @param string|array|object|integer|null $data
 *   Data to be cached persistent
 *
 * @return null
 *   Always returns null
 */
function idfix_api_cache_set_persistent($cid, $data) {
  return _idfix_cache_worker($cid, $data, TRUE);
}

/**
 * Get a value from the cache.
 *
 * Both caches are checked. Static and Sql.
 * If the Sql value is found it's stored in the static cache.
 *
 * @param string $cid
 *   Unique cache identifier
 *
 * @return string|array|object|integer|null
 *   Cached data
 */
function idfix_api_cache_get($cid) {
  return _idfix_cache_worker($cid);
}

/**
 * Worker function for static and sql caching
 *
 * This makes it easier for us to inmplement an other caching
 * strategy later on. (Other bin maybe?)
 *
 * Notes:
 * 1. This cache is language aware
 * 2. Uses drupal cache settings
 *
 * @param string $cid
 *   Unique cache identifier
 * @param string|array|object|integer|null $data
 *   Data to be cached.
 * @param bool $persistent
 *   Store persistent betwee pagecalls
 *
 * @return string|array|object|integer|null
 *   Cached data
 */
function _idfix_cache_worker($cid, $data = NULL, $persistent = FALSE) {
  global $language;
  // Build the key based on current language.
  $cache_key = __FUNCTION__ . $cid . $language->language;
  $return = NULL;
  // Use the new D7 static cache construction.
  $static_cache = &drupal_static($cache_key);
  // Only use persistent tablecaching in production mode!
  $table_caching = (boolean) (variable_get('idfix_debug_level', IDFIX_TRAIL_ERROR) == IDFIX_TRAIL_PRODUCTION);

  // Return data is $data is NULL.
  if (is_null($data)) {
    // Check the static cache first.
    if (!is_null($static_cache)) {
      $return = $static_cache;
    }
    elseif (($temp = cache_get($cache_key)) and $table_caching) {
      $return = $temp->data;
      $static_cache = $return;
    }
  }
  // Store the data.
  else {
    $static_cache = $data;
    if ($persistent and $table_caching) {
      cache_set($cache_key, $data);
    }
  }
  return $return;
}
