<?php

/**
 * @file
 * Agile development tool for building backoffice applications
 *
 * Idfix is basicly an API for building the basics of a configuration
 * driven system. Its the succesor of Asterix. Asterix was only available
 * for Drupal 6, while IDFix is built upon the Drupal 7 api's.
 *
 * Note: that this is not just an adapted version of Asterix! The IDFix system
 * is highly modified and newly architectured!
 *
 * This module file will only contain implemenations of drupal hooks
 * All IDFix API functionality is found in the include files
 */

// Reading, loading and caching of configurations.
require_once 'includes/idfix_loading.inc';

// The IDFix event system, wrapper around the drupal hook system.
require_once 'includes/idfix_events.inc';

// All communication with the datastore.
require_once 'includes/idfix_storage.inc';

// Debugging and profiling functions and UI.
require_once 'includes/idfix_debug.inc';

// Generic interface for caching.
require_once 'includes/idfix_cache.inc';

// Evenhandlers standard.
require_once 'includes/idfix_eventhandlers.inc';

/**
 * Implements hook_help.
 *
 * Displays help and module information.
 *
 * @param string $path
 *   Which path of the site we're using to display help
 * @param array $arg
 *   Array that holds the current path as returned from arg() function
 */
function idfix_help($path, $arg) {
  switch ($path) {
    case "admin/help#idfix":
      return '<p>' . t("Agile development tool for building backoffice applications.") . '</p>';
      break;
  }
}

/**
 * Implements hook_permission().
 *
 * @return array
 *   List of permissions
 */
function idfix_permission() {
  return array(
    'administer idfix' => array(
      'title' => t('Administer IDFix'),
      'description' => t('Adminster IDFix settings and show profiling- and debugging information.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_page_alter().
 *
 * @return string
 *   Full HTML table with debug and profiler information
 */
function idfix_page_alter(&$page) {
   // Never show info in production mode!
  if (_idfix_debug_level() == IDFIX_TRAIL_PRODUCTION) {
    return '';
  }

  // This is the last possibility to show a message.
  idfix_debug(__function__, NULL, IDFIX_TRAIL_DEBUG);

  $header = array(t('timer'), t('type'), t('message'), t('parameters'));
  $info = idfix_debug(NULL, NULL, IDFIX_TRAIL_PRODUCTION);
  $retval = '';
  if (is_array($info) and count($info) > 0) {
    $variables = array('header' => $header, 'rows' => $info);
    $retval .= theme('table', $variables);
  }
  if (_idfix_profiler()) {
    $retval .= idfix_profiler('', 'render');
  }

  $page['footer']['idfix'] = array(
    '#type' => 'markup',
    '#markup' => $retval,
  );

}

/**
 * Implements hook_init().
 */
function idfix_init() {
  // Just to show a timestamp.
  idfix_debug(__function__, NULL, IDFIX_TRAIL_DEBUG);
}

/**
 * Implements hook_menu().
 *
 * @return array
 *   List of menuitems
 */
function idfix_menu() {
  $items = array();
  $items['admin/config/system/idfix'] = array(
    'title' => 'IDFix',
    'description' => 'Configure IDFix settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('idfix_settings_form'),
    'access arguments' => array('administer idfix'),
    'file' => 'includes/idfix_settings.inc',
    );
  return $items;
}
